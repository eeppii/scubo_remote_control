#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include <WProgram.h>
#endif

#include <Servo.h> 
#include <ros.h>
#include <std_msgs/UInt16.h>

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

ros::NodeHandle nh; //instantiate node handle, set up the serial port communication

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(); //default adress 0x40 called

int channel=0 ; //breakout-board position 0

/*void setServoPulse(uint8_t n, double pulse) {
  double pulselength;
  pulse /=1000000;
  pulselength = 1000000;   // 1,000,000 us per second
  pulselength /= 60;   // 60 Hz
  pulselength /= 4096;  // 12 bits of resolution
  pulse *= 1000;
  pulse /= pulselength;
  pwm.setPWM(n, 0, pulse);
}*/

void pwm_cb( const std_msgs::UInt16& cmd_msg){
  pwm.setPWM(channel,0, cmd_msg.data); //PWM signal, state where it goes high, state where it goes low, 0-4095
}

ros::Subscriber<std_msgs::UInt16> sub("pwm_signal", pwm_cb); //definition topic and callback function

void setup() {
  nh.initNode();
  nh.subscribe(sub);
  
  pwm.begin(); 
  pwm.setPWMFreq(60); //60 Hz PWM signal
}


void loop() {
  nh.spinOnce();
  delay(1);
}
