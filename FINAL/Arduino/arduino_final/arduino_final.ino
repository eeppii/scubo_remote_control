//johann, 22. april 2016

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include <WProgram.h>
#endif

#include <Servo.h> 
//#define USE_USBCON //uncomment this when running on the UDOO
#include <ros.h>
#include <ArduinoHardware.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt16MultiArray.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//ros::NodeHandle nh; //instantiate node handle, set up the serial port communication
ros::NodeHandle_<ArduinoHardware, 2, 2, 150, 150> nh; // from: http://answers.ros.org/question/28890/using-rosserial-for-a-atmega168arduino-based-motorcontroller/
// note: first two numbers: Anzahl Publisher/Subscriber , second two numbers ar buffersize

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(); //default adress 0x40 called, adress of break-out board

std_msgs::String str_msg;

ros::Publisher chatter("leak_signal", &str_msg); //definition leakage topic, string

char dry[]="Everything is dry, keep going!";

char wet[]="Water leakage, manual abort!";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void pwm_cb( const std_msgs::UInt16MultiArray& cmd_msg){ // callback function of thruster allocation
  
  for(int i=0; i<8; i++) { //runs every thrusters via for-loop
     
     pwm.setPWM(i,0, cmd_msg.data[i]); //PWM signal, state where it goes high, state where it goes low, 0-4095, deadband 351-362
  
  }
  
}

ros::Subscriber<std_msgs::UInt16MultiArray> sub("pwm_signal", pwm_cb); //definition PWM topic and callback function

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup() { // this function runs first and only one time on each upload

  delay(10000); // initialization after approximately 10 seconds
  
  nh.initNode();
  
  nh.advertise(chatter); // for leakage sensor, string

  nh.subscribe(sub); // for thruster allocation
    
  pwm.begin();
  
  pwm.setPWMFreq(60); //60 Hz PWM signal
  
  for(int j=0;j<8;j++) {  //initialize every thruster via channel (0-7) with a for-loop 
  
    pwm.setPWM(j,0,351);
    
  }
  
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void loop() {
  nh.spinOnce();
  
  //defining the pins on the UDOO
  int s1=analogRead(A0);
  int s2=analogRead(A1);
  int s3=analogRead(A2);
  int s4=analogRead(A3);
  
  // transformation into voltage signal
  float v1=s1*(5.0/1023,0);
  float v2=s2*(5.0/1023,0);
  float v3=s3*(5.0/1023,0);
  float v4=s4*(5.0/1023,0);

  if(v1>3 ||v2>3||v3>3||v4>3) { // action if water leakage
   
   str_msg.data=wet;
   chatter.publish(&str_msg);
   
  }
  
  
  else { // action if no water leakage
    
    str_msg.data=dry;
    chatter.publish(&str_msg);
    
  }
   
  delay(1); // change this to 200 (ms) if running on the UDOO
   
}
