#ReadMe für: cam treiber, cam node, cam parameter anpassen, cam data volume auslesen

#installiere treiber für ueye cam
Folge wie in ReadMe von ueye angegeben. Folge den Schritten unter "Quick Start"
Bemerkungen:
-Kopiere alle files in eigenen Workspace
-1 Zeile Code (in ReadMe) um Treiber zu installieren: Aufpassen! Wähle hier die usb-variante!
-1 Zeile Code (in ReadMe) um es zu starten: Hier auch usb variante wählen.
Falls Fehler auftreten: Evt nötig von Hand bei einem angegebenen File von Hand
die Datei auf "als Exe ausführen zulassen" ankreuzen. (Frage Thomas...)


#installire ueye_cam node
hole node-ordner von bitbucked (Scubo Repo)
In Workspace_ueye: src: dann ordner ablegen
catkin_make
source devel/setup.bash
dann starte launchfile rgb8.launch

#setze ueye_cam node auf (noch aufschreiben)
starten mit: roslaunch rgb8.launch
ACHTUNG: zu unterst in rgb8.launchfile wurde remapping hinzugefügt,
dass topic als Master publiziert wird

#setzte rqt_reconfigure auf um parameter verstellen zu können
1. erstelle Workspace_rqt_reconfigure
2. erstelle src Ordner
3. download von http://wiki.ros.org/rqt_reconfigure -> brauche von all den nodelets nur den rqt_reconfigure ordner
4. catkin_make
5. source devel/setup.bash
6. rosrun rqt_reconfigure rqt_reconfigure 

#videostream ansehen:
rosrun image_view image_view image:=/cameratopicname _image_transport:=compressed

#data volume auslesen:
rostopic bw "rostopic name"
für frontcam-> rostopic bw /master/image_raw/compressed
oder auch: rosparam set enable_statistics true
dann ros nodes starten und Hz unter rqt_graph betrachten

#Ergebnisse aus Data Traffic:
ueye froncatm auf USB3.0
usb webcam auf 640x480p: 1MB/s bei 28Hz
frontcam auf 1600x1200p: 3MB/s bei 10Hz
