all steps must be executed on each machine of the network (order does not matter)

in this example, the following machines are connected:

master: scubo-nuc, ip adress 10.0.0.1
2nd machine: scubo-laptop, ip adress 10.0.0.2

--------------------------------------------------------------------------------------------------------------

if no router is used, ip adresses must be assigned manually.

click on the WLAN icon and go to "edit connections"

select ethernet, click edit, go to IPv4 Settings

set method to "manual" and assign an IP adress, for example 10.0.0.1 on the master and 10.0.0.2 on 2nd machine

(this must be set back to automatic if connecting to the internet via ethernet!)

--------------------------------------------------------------------------------------------------------------

in terminal, type "sudo gedit /etc/hosts"

in gedit, add the following new line to the list on top (before the IPv6 hosts):

(ip adress of the other machine) (space) (name of the other machine) (space) (custom short name for other machine)

the name of the other machine can be found by clicking on the gear-wheel in the top-right corner and selecting "about this computer"
any custom name can be added to refer to this ip adress

example:
on the master, add: 10.0.0.2 scubo-laptop scubo-laptop
on 2nd machine, add: 10.0.0.1 scubo-nuc scubo-nuc

save and close the document

in terminal, test connection with "ping scubo-nuc" and "ping scubo-laptop" respectively

--------------------------------------------------------------------------------------------------------------

in terminal, on the master, execute the following lines:

export ROS_HOSTNAME=scubo-nuc
export ROS_MASTER_URI=http://scubo-nuc:11311		// "scubo-nuc" refers to the ip adress written to the hosts file. 11311 is the default port for ros networks
export ROS_IP=10.0.0.1

on the 2nd machine, type:

export ROS_HOSTNAME=scubo-laptop   
export ROS_MASTER_URI=http://scubo-nuc:11311		// this line stays the same on every machine
export ROS_IP=10.0.0.2

--------------------------------------------------------------------------------------------------------------

run roscore on the master

nodes running on either machine should now connect to the same master and see each other

--------------------------------------------------------------------------------------------------------------

remark: it seems the "export ROS..." commands only affect the current shell, and need to be executed in every shell before running a node