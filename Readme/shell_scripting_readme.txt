Johann Diep, 24 March 2016, Scubo

Shell Scripting 

1. mkdir shell-scripts // create a directory for all shell scripts

2. cd shell-scripts

3. vim script_name.sh // wenn n�tig, vorher sudo apt-get install vim, script interface

4. press INSERT

5. #!/bin/sh

6. ... // Commands

7. press ESC

8. : wq /& saves the file

9. chmod +x script_name.sh // executable permission

10. ./test1.sh // run it

Autostart

Step 1: Write a script which contains ros commands and save it as script.sh

#! /bin/sh 
source /opt/ros/indigo/setup.sh 
roscore
...

Note the second line source /opt/ros/indigo/setup.sh this is the path of ros bash. We need to 
source ros setup in our script before we use any of the ros commands. 

Step 2: Make the script executable by using command 
sudo chmod +x /path/to/script.sh 

Step 3: Go to System, Preferences, Startup Application 

Step 4: click on add application or add tab, a window would popup, add random name.

Step 5: On the given window in the command tab type 
gnome-terminal -x /path/to/script.sh 

Step 7: On the terminal Edit, Profile Preferences, Title and Command tab select when command 
exits hold the terminal open 

Step 8: reboot system and your application would auto start on boot every time. 