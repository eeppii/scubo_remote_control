ZU: CONNECTNODE (Thomas)

INFO ZUR NODE:
Diese Node subscribed bei den published Messages der Spacenav_node (Twist)
und publisht diese wieder als "pwm_signal"", welche von der arduino_node (Johann) gelesen werden k�nnen.
Dies in einem 8er-Array, mit Index=angesteuerter Thruster
Am besten vor connectnode die spacenav_node starten

-> Der ganze Feedforward-Controller von Maus-Stellung auf Thruster-Befehle ist in dieser node implementiert!

!!!
INFO ZUR BENUTZUNG:
bei //SCALING-FAKTOREN F�R MAUS-INPUT-SIGNALE kann der Benutzer direkt Scalin Faktoren f�r die einzelnen
Achsen festelgen. z.b. wenn sucbo zu stark auf rotation reagiert etc. -> Gr�ssenordnung 1 beachten!
bei //DOMINANT MODE EIN (TRUE) ODER AUS (FALSE) kann eingestellt werden ob immer nur eine (die dominante) Bewegung
ausgelesen werden soll oder alle gleichzeitig
!!!

INFO ZUR BENENNUNG:
connectpkg ist der name des package
connectnode ist der name der node
connectexe ist die exe (executable) der node

#INSTALLIEREN DER CONNECTNODE
1. Erstelle "Workspace_connectnode"
2. Erstelle in diesem workspace den Ordner "src"
3. Lege in diesem src-Ordner den Ordner der Node ab (source: Bitbucket)

#STARTE DIE CONNECTNODE
1. per cd-command in Workspace_connectnode
2. catkin_make
3. source devel/setup.bash
4. rosrun connectpkg connectexe
(die node sollte jetzt laufen)

#�BERPR�FE OB NODE L�UFT
rostopic echo /pwm_signal
(Dies ist das von der node publizierte pwm-signal,
wenn die Maus bewegt wird sollten sich die Werte �ndern;)
->Die spacenav_node muss daf�r am laufen sein!