ZU: SPACENAV_NODE

Info zur Node: Diese Node liest die Position der 3D Maus von "3dconnection" aus.
Es empfiehlt sich mit der twist ausgabe zu arbeiten, da diese einen linear und angular vektor ausgibt.
linear Vektor: auslenkung in xyz und angular: drehung um xyz.

#INSTALLIEREN DER SPACENAV_NODE
1. Erstelle Workspace z.B. "Workspace_dreidmaus"
2. erstelle in diesem Workspace den Ordner "src"
3. Lade die Node unter "http://wiki.ros.org/spacenav_node" herunter
4. Lege den gerade gedwonloadeten Unterordner "spacenav_node" in dem Ordner "src" ab
(Alle anderen gedwonloadeten Unterordner wie joy, ps3joy, wiimote, joystick_drivers k�nnen gel�scht werden)
5. gehe per cd-command in den Workspace_dreidmaus Ordner (nicht in den src Ordner!)
6. sudo apt-get install ros-indigo-spacenav-node
(Falls jade installiert: anstatt -indigo- verwende -jade-)
7. gehe per cd-command in den Workspace_dreidmaus, wieder nicht in src
8. catkin_make
9. source devel/setup.bash

#STARTE DIE SPACENAV_NODE
1. per cd-command: Workspace_dreidmaus/src/spacenav_node/launch
2. roslaunch classic.launch
(die node sollte jetzt laufen)

#KONTROLLE OB SPACENAV_NODE L�UFT
1. rostopic echo /spacenav/twist


