#include <iostream>
#include <array>
using namespace std;

void tau_TtoF_T(const double * const tau_T, double * const F_T)
{
	 int m1[] = {0,  0,  0,  0,  1,  0,
	             1,  0,  0,  0,  0,  1,
                 1,  1,  0,  1,  0,  0,
				 0,  1,  0,  1,  1,  1,
				 0,  0,  1,  1,  0,  0,
				 1,  0,  1,  1,  1,  1,
				 1,  1,  1,  0,  1,  0,
				 0,  1,  1,  0,  0,  1};

	 int m2[] = {1,  1,  1,  1,  0,  1,
	 	         0,  1,  1,  1,  1,  0,
	             0,  0,  1,  0,  1,  1,
				 1,  0,  1,  0,  0,  0,
	 			 1,  1,  0,  0,  1,  1,
				 0,  1,  0,  0,  0,  0,
				 0,  0,  0,  1,  0,  1,
				 1,  0,  0,  1,  1,  0};

	 for(int l=0; l<8; l++)
		 F_T[l] = 0;

	 for(int i=0; i<6; i++)
	 	 	{
	 	 		if (tau_T[i]>0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 				F_T[l] += m1[l*6+i]*tau_T[i];
	 	 		}

	 	 		else if(tau_T[i]<0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 			 	F_T[l] -= m2[l*6+i]*tau_T[i];
	 	 		}
	 	 	}

}

 int main(int argc, char** argv)
 {
	 double tau_T[] = {0, 0, 0, 4.8, -1, 0};
	 double F_T[] = {0, 0, 0, 0, 0, 0, 0, 0};

	 tau_TtoF_T(tau_T,F_T);

	 cout<<"F_T = [";
	 for(int l=0; l<8; l++)
		 cout<<F_T[l]<<" ";
	 cout<<"]"<<endl;

	 return 0;
 }
