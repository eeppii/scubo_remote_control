#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/scubo/FINAL/Workspace_Final/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/scubo/FINAL/Workspace_Final/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/scubo/FINAL/Workspace_Final/devel/lib:/home/scubo/FINAL/Workspace_Final/devel/lib/x86_64-linux-gnu:/opt/ros/indigo/lib/x86_64-linux-gnu:/opt/ros/indigo/lib"
export PATH="/home/scubo/FINAL/Workspace_Final/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/scubo/FINAL/Workspace_Final/devel/lib/pkgconfig:/home/scubo/FINAL/Workspace_Final/devel/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/x86_64-linux-gnu/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PWD="/home/scubo/FINAL/Workspace_Final/build"
export PYTHONPATH="/home/scubo/FINAL/Workspace_Final/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/scubo/FINAL/Workspace_Final/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/scubo/FINAL/Workspace_Final/src:$ROS_PACKAGE_PATH"