//thomas 03.03.2016

//die 3d maus node findet man unter:
//http://wiki.ros.org/spacenav_node
//gebraucht wird nur der spacenav_node ordner

// This header defines the standard ROS classes.
//Note: nicht klar ob std_msgs/String.h und sstream gebraucht werden
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/String.h>
#include <sstream>
#include <math.h>

//Wir brauchen geometry_msgs::Twist für die 3d Maus auszulesen und std_msgs::UInt16 um die Thruster anzusteuern
//Note: rosmsg show gibt uint16 data aus
//Probieren jetzt MultyArray zu brauchen um channel mit zu geben

//definiere temp als double um connection zwischen callbackfunciton und mainfunction herzustellen.
//Problem: variabeln von der callbackfct werden in der main nicht gesehen. darum globale variable temp benötigt.
double mausposit[6];
bool notaus=false;

//callback functino von subscriber aufgerufen, darum brauchts ros::spinOnce
void connectcallback(const geometry_msgs::Twist &msg) {
   //subscriber liest 3dmausdaten aus -> msg. wir ordnen diese dem temp zu um es in der main beim publisher zu verwenden 
   mausposit[0]=msg.linear.x; //0.68359375 is max output in spacemouse. mausposit is stretched to 1
   mausposit[1]=msg.linear.y; //Attention! 0.6835... must be checked for every spacemous by echo /spacenav/twist
   mausposit[2]=msg.linear.z;
   mausposit[3]=msg.angular.x;
   mausposit[4]=msg.angular.y;
   mausposit[5]=msg.angular.z;

   //Mousetuning parameters
	double deadbandmouse=0.08;
	double mousemaximum=0.68359375;

   //Spacemouse must have small Deadband. The mouse-value is stretched to 1 based on tested mousemaximum value
   for (int i = 0; i < 6; ++i)
   {
   	if (fabs(mausposit[i])<deadbandmouse) //note: same deadband for all directions
   	{
   		mausposit[i]=0;
   	}
   	else{
   		mausposit[i]=(mausposit[i]-deadbandmouse)/(mousemaximum-deadbandmouse);
   	}
   }

   //ROS_INFO verwenden um zu testen ob dieser schritt durchgeführt wird
   //ROS_INFO("got twist message, value: %f", msg.angular.x);
}

//callback function für Notaus

void connectcallbacknotaus(const std_msgs::Bool &msg) {
notaus=msg.data;


}

//void function von Yvain um Mausauslenkung auf Thruster umzuleiten
void tau_TtoF_T(const double * const tau_T, double * const F_T)
{
	 int m1[] = {0,  0,  0,  0,  1,  0,
	             1,  0,  0,  0,  0,  1,
                 1,  1,  0,  1,  0,  0,
				 0,  1,  0,  1,  1,  1,
				 0,  0,  1,  1,  0,  0,
				 1,  0,  1,  1,  1,  1,
				 1,  1,  1,  0,  1,  0,
				 0,  1,  1,  0,  0,  1};

	 int m2[] = {1,  1,  1,  1,  0,  1,
	 	         0,  1,  1,  1,  1,  0,
	             0,  0,  1,  0,  1,  1,
				 1,  0,  1,  0,  0,  0,
	 			 1,  1,  0,  0,  1,  1,
				 0,  1,  0,  0,  0,  0,
				 0,  0,  0,  1,  0,  1,
				 1,  0,  0,  1,  1,  0};

	 for(int l=0; l<8; l++)
		 F_T[l] = 0;

	 for(int i=0; i<6; i++)
	 	 	{
	 	 		if (tau_T[i]>0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 				F_T[l] += m1[l*6+i]*tau_T[i];
	 	 		}

	 	 		else if(tau_T[i]<0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 			 	F_T[l] -= m2[l*6+i]*tau_T[i];
	 	 		}
	 	 	}

}

int main(int argc, char **argv) {
  // Initialize the ROS system.
  //gebe hier der node den namen
  ros::init(argc, argv, "connectnode");

  // Establish this program as a ROS node.
  ros::NodeHandle nh;
 
 //Erstelle Subscriber, subscriber bekommt daten von der 3dmaus
	ros::Subscriber sub=nh.subscribe("spacenav/twist",1000, &connectcallback);

 //Erste Notaus-Version
	ros::Subscriber substop=nh.subscribe("stop_signal",1000, &connectcallbacknotaus);

	
	//Erstelle Publisher, publisher publisht daten für den thruster. wichtig: std_msgs::UInt16 ist von der ardunio-node vorgegeben.
	//std_msgs/UInt16 ist message-type (vom pwm_signal) und pwm_signal ist der topic-name
	ros::Publisher pub=nh.advertise<std_msgs::UInt16MultiArray>("pwm_signal",1000);
	
	//////////////WICHTIG///////////////
	////////PARAMETER HIER ANPASSEN////

	//deadbandmiddle-wert (deadband individuell, middle sollte bei allen enthalten sein)
	int deadbandmiddle=347;

	//Um Max-Schub zu begrenzen von den Thrustern: Hier Downgrade Faktor zwischen 0 und 1 wählen
	double downgrade=1;
	
	//SCALING-FAKTOREN FÜR MAUS-INPUT-SIGNALE
	//setzte Grössenordnung ca 1
	//scaling linear
	double sx=1;
	double sy=1;
	double sz=1;
	//scaling angular
	double ax=1;
	double ay=1;
	double az=1;
	//DOMINANT MODE EIN (TRUE) ODER AUS (FALSE)
	bool dominant=true;
	
	//stretch variable gibt angenähert das thrusterintervall an mit 350+/-stretch (*downgrade)
	int stretch=90;

	//Damit Thruster nicht zu schnell reagieren kann pwm_signal pro loop nur um maximal +1*upmax steigen. --> loop läuft mit ros::Rate rate(#Hz); d.h. #Hz*upmax ist die maxiamle Werterhöhung pro Sekunde
	double upmax=1;

	double thrusterdeadbandup[]={352,352,352,352,352,352,352,352};
	double thrusterthrustmax[]={440,440,440,440,440,440,440,440};
	double thrusterdeadbanddown[]={341,341,341,341,341,341,341,341};
	double thrusterthrustmin[]={250,250,250,250,250,250,250,250};
	
	///////////////////////////////////////
	//////////////WICHTIG///////////////
	
	//erstelle Platzhalter für Mausposition, Reihenfolge: LINEAR X Y Z ANGULAR X Y Z
	double tau_T[] = {0,0,0,0,0,0};
	//erstelle Platzhalter für Thrusterbefehle, Reihenfolge: Thruster 0-7 mit rechts-vorne-oben, links-vorne-oben, links-hinten-oben, rechts-hinten-oben, dann alles gleich aber mit -unten
	double F_T[] = {0, 0, 0, 0, 0, 0, 0, 0};
	//erstelle Platzhalter für Thruster Sollzustand
	double F_T_IST[] = {0, 0, 0, 0, 0, 0, 0, 0};


	//definiere hier in welcher frequenz signale publiziert werden sollen.
	ros::Rate rate(60);

while(ros::ok()){
	//erstelle hier das signal welches publiziert werden soll. wichtig: muss zu message-type von thruster passen
	std_msgs::UInt16MultiArray signal;
	signal.data.resize(8);
	
	//dominant mode
	if(dominant){
	double max=fabs(mausposit[0]);
	int index=0;
    for(int i = 1; i < 6; i++)
    {
        if(fabs(mausposit[i]) > max){
            max = fabs(mausposit[i]);
            index=i;
		}
    }
    for(int i=0;i<6;i++)
    {
		if(i!=index){
		mausposit[i]=0;
		}
	}
	}
	//teste dominant mode
	//ROS_INFO("in x it is, value: %f", mausposit[0]);
	//ROS_INFO("in y it is, value: %f", mausposit[1]);
	
	//scaling
	tau_T[0]=mausposit[0]*sx;
	tau_T[1]=mausposit[1]*sy;
	tau_T[2]=mausposit[2]*sz;
	tau_T[3]=mausposit[3]*ax;
	tau_T[4]=mausposit[4]*ay;
	tau_T[5]=mausposit[5]*az;
	
	//berechne aus mausposition die thrusteranste50uerung
	 tau_TtoF_T(tau_T,F_T);
	 
	//Teste Threshold
	//Setzte max bei 1, alles darüber wird nach unten gedrückt
	double thres=fabs(F_T[0]);
    for(int i = 1; i < 8; i++)
    {
        if(fabs(F_T[i]) > thres){
            thres = fabs(F_T[i]);
		}
    }
    if(thres>1){
	//ROS_INFO("Threshold applied, value %f", thres);
    for(int i=0;i<8;i++)
    {
		F_T[i]=F_T[i]/thres;
	}
	}

	//upmax begrenzt die Steigung von pwm_signal
	for(int i=0;i<8;i++)
	{
		if(fabs(F_T[i])>fabs(F_T_IST[i]))
		{
			F_T_IST[i]+=upmax/(stretch*downgrade)*F_T[i]/fabs(F_T[i]); //Note: F_T/fabs(F_T) gleicht das Vorzeichen (Vorwärts/Rückwärts) an und /(stretch*downgrade)
		}
		else
		{
			F_T_IST[i]=F_T[i];
		}

	}
	
	//////////////WICHTIG///////////////
	////////////////////////////////////// 

	//Notaus
	if(notaus==true){

	signal.data[0]=deadbandmiddle;
	signal.data[1]=deadbandmiddle;
	signal.data[2]=deadbandmiddle;
	signal.data[3]=deadbandmiddle;
	signal.data[4]=deadbandmiddle;
	signal.data[5]=deadbandmiddle;
	signal.data[6]=deadbandmiddle;
	signal.data[7]=deadbandmiddle;

	ROS_INFO("Emergency message, stopping Scubo!");

	}
	
	//Verknüpfung mit Notaus
	else{

	//Thrusteransteuerung auf Thrusterbefehl und skalieren 
	//Hier DEADBAND und VORZEICHEN tunen!

	for (int i = 0; i < 8; ++i)
	{
		if (F_T_IST[i]>0) //front direction
		{
			signal.data[i]=thrusterdeadbandup[i]+F_T_IST[i]*(thrusterthrustmax[i]-thrusterdeadbandup[i])*downgrade;
		}
		else if (F_T_IST[i]==0)
		{
			signal.data[i]=deadbandmiddle;
		}
		else {
			signal.data[i]=thrusterdeadbanddown[i]-F_T_IST[i]*(thrusterthrustmin[i]-thrusterdeadbanddown[i])*downgrade;

		}
	}

	ROS_INFO("All good, Scubo is diving!");

	
	}
	////////////////////////////////////////////
	////////////////////WICHTIG/////////////////


	//brauche ROS_INFO um zu checken ob der code bei dieser stelle her ankommt
	//ROS_INFO("I'm about to publish, value: %f", signal.data);
	
	//publiziere signal
	pub.publish(signal);
	
	//brauche ros::spinOnce() damit wieder erneut die callbackfct benutzt werden kann
	ros::spinOnce();
	//dieser befehl sagt es soll warten bis die next berechnung kommt (in ros::Rate rate(10) definiert)
	rate.sleep();
	
}
//Note: verwende hier unten NIE ros::spin() da man hier nur ankommt wenn node crasht (ros::ok() war false), und ein ros::spin() kommt da gar nicht gut an^^

// Send some output as a log message. (wenn man will...)

//return 0; wegen int main...
 return 0;
}
