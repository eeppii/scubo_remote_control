//thomas 03.03.2016

//die 3d maus node findet man unter:
//http://wiki.ros.org/spacenav_node
//gebraucht wird nur der spacenav_node ordner

// This header defines the standard ROS classes.
//Note: nicht klar ob std_msgs/String.h und sstream gebraucht werden
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/String.h>
#include <sstream>
#include <math.h>

//Wir brauchen geometry_msgs::Twist für die 3d Maus auszulesen und std_msgs::UInt16 um die Thruster anzusteuern
//Note: rosmsg show gibt uint16 data aus
//Probieren jetzt MultyArray zu brauchen um channel mit zu geben

//definiere temp als double um connection zwischen callbackfunciton und mainfunction herzustellen.
//Problem: variabeln von der callbackfct werden in der main nicht gesehen. darum globale variable temp benötigt.
double mausposit[6];
bool notaus=false;

//callback functino von subscriber aufgerufen, darum brauchts ros::spinOnce
void connectcallback(const geometry_msgs::Twist &msg) {
   //subscriber liest 3dmausdaten aus -> msg. wir ordnen diese dem temp zu um es in der main beim publisher zu verwenden 
   mausposit[0]=msg.linear.x;
   mausposit[1]=msg.linear.y;
   mausposit[2]=msg.linear.z;
   mausposit[3]=msg.angular.x;
   mausposit[4]=msg.angular.y;
   mausposit[5]=msg.angular.z;
   
   //ROS_INFO verwenden um zu testen ob dieser schritt durchgeführt wird
   //ROS_INFO("got twist message, value: %f", msg.angular.x);
}

//callback function für Notaus
void connectcallbacknotaus(const std_msgs::Bool &msg) {
notaus=msg.data;


}

//void function von Yvain um Mausauslenkung auf Thruster umzuleiten
void tau_TtoF_T(const double * const tau_T, double * const F_T)
{
	 int m1[] = {0,  0,  0,  0,  1,  0,
	             1,  0,  0,  0,  0,  1,
                 1,  1,  0,  1,  0,  0,
				 0,  1,  0,  1,  1,  1,
				 0,  0,  1,  1,  0,  0,
				 1,  0,  1,  1,  1,  1,
				 1,  1,  1,  0,  1,  0,
				 0,  1,  1,  0,  0,  1};

	 int m2[] = {1,  1,  1,  1,  0,  1,
	 	         0,  1,  1,  1,  1,  0,
	             0,  0,  1,  0,  1,  1,
				 1,  0,  1,  0,  0,  0,
	 			 1,  1,  0,  0,  1,  1,
				 0,  1,  0,  0,  0,  0,
				 0,  0,  0,  1,  0,  1,
				 1,  0,  0,  1,  1,  0};

	 for(int l=0; l<8; l++)
		 F_T[l] = 0;

	 for(int i=0; i<6; i++)
	 	 	{
	 	 		if (tau_T[i]>0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 				F_T[l] += m1[l*6+i]*tau_T[i];
	 	 		}

	 	 		else if(tau_T[i]<0)
	 	 		{
	 	 			for(int l=0; l<8; l++)
	 	 			 	F_T[l] -= m2[l*6+i]*tau_T[i];
	 	 		}
	 	 	}

}

int main(int argc, char **argv) {
  // Initialize the ROS system.
  //gebe hier der node den namen
  ros::init(argc, argv, "connectnode");

  // Establish this program as a ROS node.
  ros::NodeHandle nh;
 
 //Erstelle Subscriber, subscriber bekommt daten von der 3dmaus
	ros::Subscriber sub=nh.subscribe("spacenav/twist",1000, &connectcallback);

 //Erste Notaus-Version
	ros::Subscriber substop=nh.subscribe("stop_signal",1000, &connectcallbacknotaus);

	
	//Erstelle Publisher, publisher publisht daten für den thruster. wichtig: std_msgs::UInt16 ist von der ardunio-node vorgegeben.
	//std_msgs/UInt16 ist message-type (vom pwm_signal) und pwm_signal ist der topic-name
	ros::Publisher pub=nh.advertise<std_msgs::UInt16MultiArray>("pwm_signal",1000);
	
	//////////////WICHTIG///////////////
	////////PARAMETER HIER ANPASSEN////

	//deadband-wert (deadband 351-362)
	int deadband=346;

	//Um Max-Schub zu begrenzen von den Thrustern: Hier Downgrade Faktor zwischen 0 und 1 wählen
	double downgrade=1;
	
	//SCALING-FAKTOREN FÜR MAUS-INPUT-SIGNALE
	//setzte Grössenordnung ca 1
	//scaling linear
	double sx=1;
	double sy=1;
	double sz=1;
	//scaling angular
	double ax=1;
	double ay=1;
	double az=1;
	//DOMINANT MODE EIN (TRUE) ODER AUS (FALSE)
	bool dominant=true;
	
	//stretch variable gibt das thrusterintervall an mit 350+/-stretch (*downgrade)
	int stretch=50;

	//Damit Thruster nicht zu schnell reagieren kann pwm_signal pro loop nur um maximal +1*upmax steigen. --> loop läuft mit ros::Rate rate(#Hz); d.h. #Hz*upmax ist die maxiamle Werterhöhung pro Sekunde
	double upmax=1;

	///////////////////////////////////////
	//////////////WICHTIG///////////////
	
	//erstelle Platzhalter für Mausposition, Reihenfolge: LINEAR X Y Z ANGULAR X Y Z
	double tau_T[] = {0,0,0,0,0,0};
	//erstelle Platzhalter für Thrusterbefehle, Reihenfolge: Thruster 0-7 mit rechts-vorne-oben, links-vorne-oben, links-hinten-oben, rechts-hinten-oben, dann alles gleich aber mit -unten
	double F_T[] = {0, 0, 0, 0, 0, 0, 0, 0};
	//erstelle Platzhalter für Thruster Sollzustand
	double F_T_IST[] = {0, 0, 0, 0, 0, 0, 0, 0};


	//definiere hier in welcher frequenz signale publiziert werden sollen.
	ros::Rate rate(60);

while(ros::ok()){
	//erstelle hier das signal welches publiziert werden soll. wichtig: muss zu message-type von thruster passen
	std_msgs::UInt16MultiArray signal;
	signal.data.resize(8);
	
	//dominant mode
	if(dominant){
	double max=fabs(mausposit[0]);
	int index=0;
    for(int i = 1; i < 6; i++)
    {
        if(fabs(mausposit[i]) > max){
            max = fabs(mausposit[i]);
            index=i;
		}
    }
    for(int i=0;i<6;i++)
    {
		if(i!=index){
		mausposit[i]=0;
		}
	}
	}
	//teste dominant mode
	//ROS_INFO("in x it is, value: %f", mausposit[0]);
	//ROS_INFO("in y it is, value: %f", mausposit[1]);
	
	//scaling
	tau_T[0]=mausposit[0]*sx;
	tau_T[1]=mausposit[1]*sy;
	tau_T[2]=mausposit[2]*sz;
	tau_T[3]=mausposit[3]*ax;
	tau_T[4]=mausposit[4]*ay;
	tau_T[5]=mausposit[5]*az;
	
	//berechne aus mausposition die thrusteransteuerung
	 tau_TtoF_T(tau_T,F_T);
	 
	//Teste Threshold
	//Setzte max bei 1, alles darüber wird nach unten gedrückt
	double thres=fabs(F_T[0]);
    for(int i = 1; i < 8; i++)
    {
        if(fabs(F_T[i]) > thres){
            thres = fabs(F_T[i]);
		}
    }
    if(thres>1){
	//ROS_INFO("Threshold applied, value %f", thres);
    for(int i=0;i<8;i++)
    {
		F_T[i]=F_T[i]/thres;
	}
	}

	//upmax begrenzt die Steigung von pwm_signal
	for(int i=0;i<8;i++)
	{
		if(fabs(F_T[i])>fabs(F_T_IST[i]))
		{
			F_T_IST[i]+=upmax/(stretch*downgrade)*F_T[i]/fabs(F_T[i]); //Note: F_T/fabs(F_T) gleicht das Vorzeichen (Vorwärts/Rückwärts) an und /(stretch*downgrade)
		}
		else
		{
			F_T_IST[i]=F_T[i];
		}

	}
	
	//////////////WICHTIG///////////////
	////////////////////////////////////// 

	//Notaus
	if(notaus==true){

	signal.data[0]=deadband;
	signal.data[1]=deadband;
	signal.data[2]=deadband;
	signal.data[3]=deadband;
	signal.data[4]=deadband;
	signal.data[5]=deadband;
	signal.data[6]=deadband;
	signal.data[7]=deadband;

	ROS_INFO("Emergency message, stopping Scubo!");

	}
	
	//Verknüpfung mit Notaus
	else{

	//Thrusteransteuerung auf Thrusterbefehl und skalieren
	signal.data[0]=357+F_T_IST[0]*stretch*downgrade;
	signal.data[1]=357-F_T_IST[1]*stretch*downgrade;
	signal.data[2]=357+F_T_IST[2]*stretch*downgrade;
	signal.data[3]=357+F_T_IST[3]*stretch*downgrade; //vorzeichen
	signal.data[4]=357+F_T_IST[4]*stretch*downgrade; //vorz
	signal.data[5]=357-F_T_IST[5]*stretch*downgrade;
	signal.data[6]=357-F_T_IST[6]*stretch*downgrade;
	signal.data[7]=357-F_T_IST[7]*stretch*downgrade; //vorz

	ROS_INFO("All good, Scubo is diving!");

	
	}
	////////////////////////////////////////////
	////////////////////WICHTIG/////////////////


	//brauche ROS_INFO um zu checken ob der code bei dieser stelle her ankommt
	//ROS_INFO("I'm about to publish, value: %f", signal.data);
	
	//publiziere signal
	pub.publish(signal);
	
	//brauche ros::spinOnce() damit wieder erneut die callbackfct benutzt werden kann
	ros::spinOnce();
	//dieser befehl sagt es soll warten bis die next berechnung kommt (in ros::Rate rate(10) definiert)
	rate.sleep();
	
}
//Note: verwende hier unten NIE ros::spin() da man hier nur ankommt wenn node crasht (ros::ok() war false), und ein ros::spin() kommt da gar nicht gut an^^

// Send some output as a log message. (wenn man will...)

//return 0; wegen int main...
 return 0;
}
